//Cop Final Project
//@author: John Ryan

// Common Items

var ancientTome =
    {
        type: "Tome",
        phase: "Movement",
        skillCheckType: "Lore",
        skillCheckModifier: -1,
        price: 4,
        passResult: "Draw one spell and discard Ancient Tome",
        failResult: "Nothing Happens",
        description: "Exhaust and spend 2 movement points to make a Lore(-1) check. If you pass, draw one Spell and discard Ancient Tome. If you fail, nothing happens"
    };

var automatic =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 4,
        price: 5,
        hands: 1,
        description: ".45 automatic"
    };

var axe =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: [2, 3],
        price: 3,
        hands: 1,
        description: "+2 to Combat Checks (+3 instead, if your other hand is empty."
    };

var bullWhip =
    {
        type: "Physical Weapon",
        phase: "Any",
        skillCheckType: "Combat",
        skillCheckModifier: 1,
        price: 2,
        hands: 1,
        description: "Any Phase: Exhaust to re-roll 1 die after making a Combat check."
    };

var cavalrySaber =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 2,
        price: 3,
        hands: 1
    };

var cross =
    {
        type: "Magical Weapon",
        phase: "Movement",
        skillCheckType: ["Combat", "Horror"],
        skillCheckModifierCombat: [0, 3],
        skillCheckModifierHorror: 1,
        price: 3,
        hands: 1,
        description: "+0 to Combat checks (+3 if Opponent is Undead)"
    };

var darkCloak =
    {
        phase: "Movement",
        skillCheckType: "Evade",
        skillCheckModifier: 1,
        price: 2
    };

var dynamite =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 8,
        price: 4,
        hands: 2,
        description: "Discard after use"
    };

var derringer =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 2,
        price: 3,
        hands: 1,
        description: ".18 Derringer cannot be lost or stolen, unless you choose to allow it."
    };

var food =
    {
        phase: "Any",
        description: "Any Phase: Discard Food to reduce any Stamina loss by 1."
    };

var knife =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 1,
        price: 2,
        hands: 1
    };

var lantern =
    {
        price: 3,
        description: "+1 to Luck Checks"
    };

var luckyCigaretteCase =
    {
        phase: "Any",
        price: 1,
        description: "Any Phase: Discard Lucky Cigarette case to re-roll any one skill check."
    };

var mapOfArkham =
    {
        phase: "Movement",
        price: 2,
        description: "Movement: Exhaust to get 1 extra movement point."
    };

var motorcycle =
    {
        phase: "Movement",
        price: 4,
        description: "Movement: Exhaust to get 2 extra movement points."
    };

var oldJournal =
    {
        type: "Tome",
        phase: "Movement",
        skillCheckType: "Lore",
        skillCheckModifier: -1,
        price: 1,
        passResult: "Gain 3 Clue tokens and discard Old Journal,",
        failResult: "Nothing happens.",
        description: "Movement: Exhaust and spend 1 movement point to make a Lore(-1) check. If you pass, gain 3 Clue tokens and discard Old Journal. If you fail, nothing happens."
    };

var researchMaterials =
    {
        phase: "Any",
        price: 1,
        description: "Any Phase: Discard Research Materials instead of spending 1 Clue token."
    };

var revolver =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 3,
        price: 4,
        hands: 1,
        description: ".38 Revolver"
    };

var rifle =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 5,
        price: 6,
        hands: 2
    };

var shotgun =
    {
        type: "Physical Weapon",
        phase: "Any",
        skillCheckType: "Combat",
        skillCheckModifier: 4,
        price: 6,
        hands: 2,
        description: "Any phase: When using Shotgun in Combat, all 6s rolled count as 2 successes."
    };

var tommyGun =
    {
        type: "Physical Weapon",
        phase: "Movement",
        skillCheckType: "Combat",
        skillCheckModifier: 6,
        price: 7,
        hands: 2
    };

var whiskey =
    {
        phase: "Any",
        description: "Any Phase: Discard Whiskey to reduce any Sanity loss by 1"
    };

var commonItemManifest =
    {
        ancientTome: 2,
        automatic: 2,
        axe: 2,
        bullWhip: 2,
        cavalrySaber: 2,
        cross: 2,
        darkCloak: 2,
        derringer: 2,
        dynamite: 2,
        food: 2,
        knife: 2,
        lantern: 2,
        luckyCigaretteCase: 2,
        mapOfArkham: 2,
        motorcycle: 2,
        oldJournal: 2,
        researchMaterials: 2,
        revolver: 2,
        rifle: 2,
        shotgun: 2,
        tommyGun: 2,
        whiskey: 2
    };

// Unique Items

var alienStatue =
    {
        price : 5,
        description: "Movement: Exhaust and spend 2 movement points and 1 sanity to roll a die. If the die is a success, draw 1 spell or gain 3 Clue tokens. If it is a failure, lose 2 Stamina."
    };

var ancientTablet =
    {
        price: 8,
        description: "Movement: Spend 3 movement points and discard Ancient Tablet to roll 2 dice. For every success rolled, draw 1 Spell. For every failure rolled, gain 2 Clue tokens."
    };

var blueWatcherOfThePyramid =
    {
        price: 4,
        description: "Any Phase: Lose 2 Stamina and discard Blue Watcher of the Pyramid to automatically succeed at a Combat Check, or a Fight or Lore check made to close a gate. This cannot be used against an Ancient One."
    };

var bookOfDzyan =
    {
        type: "Tome",
        price: 3,
        description: "Movement: Exhaust and spend 2 movement points to make a Lore(-1) check. If you pass, draw 1 Spell, lose 1 Sanity, and put 1 Stamina token from the bank on Book of Dzyan. If there are 2 Stamina tokens on it, discard Book of Dzyan. If you fail, nothing happens."
    };

var cabalaOfSaboth =
    {
        type: "Tome",
        price: 5,
        description: "Movement: Exhaust and spend 2 movement points to make a Lore(-2) check. If you pass, draw 1 Skill and discard Cabala of Saboth. If you fail, nothing happens."
    };

var cultesDesGoules =
    {
        type: "Tome",
        price: 3,
        description: "Movement: Exhaust and spend 2 movement points to make a Lore(-2) check. If you pass, draw 1 Spell and gain 1 Clue token, but lose 2 Sanity and discard Cultes des Goules. If you fail, nothing happens."
    };

var dragonsEye =
    {
        price: 6,
        description: "Any Phase: Exhaust and lose 1 Sanity after drawing a Gate or Location card to draw a new card in its place."
    };

var elderSign =
    {
        price: 5,
        description: "Any Phase: When sealing a gate, lose 1 Stamina and 1 Sanity and return this card to the box. You do not need to make a Skill check or spend any Clue tokens to seal the gate. In addition, remove one doom token from the Ancient One's doom track."
    };

var enchantedBlade =
    {
        type: "Magical Weapon",
        price: 6,
        hands: 1,
        skillCheckType: "Combat",
        skillCheckModifier: 4
    };

var enchantedKnife =
    {
        type: "Magical Weapon",
        price: 5,
        hands: 1,
        skillCheckType: "Combat",
        skillCheckModifier: 3
    };

var enchantedJewelry =
    {
        price: 3,
        description: "Any Phase: Put 1 Stamina token from the bank on Enchanted Jewelry to avoid losing 1 Stamina. If there are 3 Stamina tokens on it, discard Enchanted Jewelry."
    };

var fluteOfTheOuterGods =
    {
        price: 8,
        description: "Any Phase: Lose 3 Sanity and 3 Stamina and discard Flute of the Outer Gods before making a Combat check to defeat all monsters in your current area. This does not affect Ancient Ones."
    };

var gateBox =
    {
        price: 4,
        description: "Any Phase: When you return to Arkham from an Other World, you can return to any location with an open gate, not just those leading to the Other World you were in."
    };

var healingStone =
    {
        price: 8,
        description: "Upkeep: Exhaust to gain 1 Stamina or 1 Sanity.\n\
                      Discard this card if the Ancient One awakens."
    };

var holyWater =
    {
        type: "Magical Weapon",
        price: 4,
        hands: 2,
        skillCheckType: "Combat",
        skillCheckModifier: 6,
        description: "Discard after use."
    };

var lampOfAlhazred =
    {
        type: "Magical Weapon",
        price: 7,
        hands: 2,
        skillCheckType: "Combat",
        skillCheckModifier: 5
    };

var namelessCults =
    {
        type: "Tome",
        price: 3,
        description: "Movement: Exhaust and spend 1 movement point to make a Lore(-1) check. If you pass, draw 1 Spell, lose 1 Sanity, and discard Nameless Cults. If you fail, nothing happens."
    };

var necronomicon =
    {
        type: "Tome",
        price: 6,
        description: "Movement: Exhaust and spend 2 movement points to make a Lore(-2) check. If you pass, draw 1 Spell and lose 2 Sanity. If you fail, nothing happens."
    };

var obsidianStatue =
    {
        price: 4,
        description: "Any Phase: Discard Obsidian Statue to cancel all Stamina or Sanity loss being dealt to you from 1 source."
    };

var pallidMask =
    {
        price: 4,
        skillCheckType: "Evade",
        skillCheckModifier: 2
    };

var powderOfIbn_Ghazi =
    {
        type: "Magical Weapon",
        price: 6,
        hands: 2,
        skillCheckType: "Combat",
        skillCheckModifier: 9,
        description: "Lose 1 Sanity and discard after use."
    };

var rubyOfRyleh =
    {
        price: 8,
        description: "Movement: You get 3 extra movement points."
    };

var silverKey =
    {
        price: 4,
        description: "Any Phase: Put 1 Stamina token from the bank on Silver Key before making an Evade check to automatically pass it. Discard Silver Key after using it if there are 3 Stamina tokens on it."
    };

var swordOfGlory =
    {
        type: "Magical Weapon",
        price: 8,
        hands: 2,
        skillCheckType: "Combat",
        skillCheckModifier: 6,
    };

var theKingInYellow =
    {
        type: "Tome",
        price: 2,
        description: "Movement: Exhaust and spend 2 movement points to make a Lore(-2) check. If you pass, gain 4 Clue tokens, lose 1 Sanity, and discard The King in Yellow. If you fail, nothing happens."
    };

var wardingStatue =
    {
        price: 6,
        description: "Any Phase: Discard Warding Statue after failing a Combat check to reduce the monster's combat damage to 0 Stamina. This can also be used to cancel an Ancient One's entire attack for 1 turn."
    };

var uniqueItemManifest =
    {
        alienStatue: 1,
        ancientTablet: 1,
        blueWatcherOfThePyramid: 1,
        bookOfDzyan: 1,
        cabalaOfSaboth: 2,
        cultesDesGoules: 2,
        dragonsEye: 1,
        elderSign: 4,
        enchantedBlade: 2,
        enchantedKnife: 2,
        enchantedJewelry: 1,
        fluteOfTheOuterGods: 1,
        gateBox: 1,
        healingStone: 1,
        holyWater: 4,
        lampOfAlhazred: 1,
        namelessCults: 2,
        necronomicon: 1,
        obsidianStatue: 1,
        pallidMask: 1,
        powderOfIbn_Ghazi: 2,
        rubyOfRyleh: 1,
        silverKey: 1,
        swordOfGlory: 1,
        theKingInYellow: 2,
        wardingStatue: 1
    };





var Amanda = {
    name: "Amanda",
    title: "the Student",
    sanity: 5,
    stamina: 5,
    home: "Bank of Arkham",
    focus: 3,
    speed: [1, 2, 3, 4],
    sneak: [4, 3, 2, 1],
    fight: [1, 2, 3, 4],
    will: [4, 3, 2, 1],
    lore: [1, 2, 3, 4],
    luck: [4, 3, 2, 1],
    spd_snk:0,
    fgt_wil:0,
    lor_luk:0
};

var Investigator = {
    name:"",
    title:"",
    sanity:0,
    stamina:0,
    home:"",
    focus:3,
    speed: [1, 2, 3, 4],
    sneak: [4, 8, 2, 1],
    fight: [1, 2, 3, 4],
    will: [4, 3, 7, 1],
    lore: [1, 2, 3, 4],
    luck: [6, 3, 2, 1],
    spd_snk:0,
    fgt_wil:0,
    lor_luk:0
    
};





// Investigator List
var investigatorList =
    {
        "Amanda" :Amanda
    };


// Global Variables
var selectedInvestigator = null;
var selectedSkillCheck = "";

var selectedItem = "";
































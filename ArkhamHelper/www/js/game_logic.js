$(document).ready(function(){
    $("#selectinvestigator").on("click",function(){
        selectedInvestigator = $("#invselect option:selected").val();
        loadInvsPage();
    });
    
    $("#selectfightwill").change(function(){
        investigatorList[selectedInvestigator].fgt_wil = $("#selectfightwill option:selected").val();
    })
    
    $("#selectspeedsneak").change(function(){
        investigatorList[selectedInvestigator].spd_snk = $("#selectspeedsneak option:selected").val();
    })
    
    $("#selectloreluck").change(function(){
        investigatorList[selectedInvestigator].lor_luk = $("#selectloreluck option:selected").val();
    })
    
    $(".uib_skillcheck").on("click",function(){
        selectedSkillCheck = $(this).attr("value");
        loadSkillPage(selectedSkillCheck);
    })
    
    $(".mod-total").change(function(){
        loadSkillPage(selectedSkillCheck);
    })
    
});

function loadInvsPage (){
    var inv = investigatorList[selectedInvestigator];
    //here start building the lists on the page
    var $fiwi = $("#selectfightwill");
    var $spsn = $("#selectspeedsneak");
    var $lolu = $("#selectloreluck");
    
    for (i=0;i<4;i++){        
        $fiwi.append($('<option/>', {
            value: i,
            text: inv.fight[i].toString()+"/"+inv.will[i].toString(),
            selected : i==0
        }));
        
        $spsn.append($('<option/>', {
            value: i,
            text: inv.speed[i].toString()+"/"+inv.sneak[i].toString()
        }));
        
        $lolu.append($('<option/>', {
            value: i,
            text: inv.lore[i].toString()+"/"+inv.luck[i].toString()
        }));
    }
    
    $("#focus h").html("Focus: " + investigatorList[selectedInvestigator].focus);
}

function loadSkillPage (type){
    
    var inv = investigatorList[selectedInvestigator];
    
    var base = 0;
    var pm = $("#plus-minus option:selected").val();
    var modifier = $("#modifier-amount option:selected").val();
    
    switch(type) {
            case "speed":
                base = inv.speed[inv.spd_snk];
                $("#skill-score").html("Speed " + base);
            break;
            
            case "sneak":
            base = inv.sneak[inv.spd_snk];
                $("#skill-score").html("Sneak " + base);
            break;
            
            case "fight":
            base = inv.fight[inv.fgt_wil];
                $("#skill-score").html("Fight " + base);
            break;
            
            case "will":
            base = inv.will[inv.fgt_wil];
                $("#skill-score").html("Will " + base);
            break;
            
            case "lore":
            base = inv.lore[inv.lor_luk];
                $("#skill-score").html("Lore " + base);
            break;
            
            case "luck":
            base = inv.luck[inv.lor_luk];
                $("#skill-score").html("Luck " + base);
            
    }    
        
    if (pm === "+") {
        $("#result-info").html("Roll " + (base + +modifier) + " dice");
    }
    else {
        if((base - +modifier)<1){
            $("#result-info").html("Automatic Fail");
        }else{
            $("#result-info").html("Roll " + (base - +modifier) + " dice");
        }
    }
   
}